jQuery(document).ready(function () {

    /**
     * Предзагрузка изображения.
     */
    src = jQuery('body')
        .css('background-image')
        .replace('url(','')
        .replace(')','')
        .replace(/\"/gi, "")
    ;
    jQuery('<img>').attr('src', src);

    /**
     * Установка года в подвале.
     */
    setTimeout(function () {
        jQuery('#footer-year').text(', ' + (new Date()).getFullYear());
    }, 3);

    /**
     * Добавление эффектов.
     */
    setTimeout(function () {
        jQuery('#layout').fadeOut(3000);
    }, 500);
    setTimeout(function () {
        jQuery('#main-headers').animate({
            'margin-top': '4%',
        }, 3000, 'swing');
    }, 1500);

    /**
     * Добавление параллакса.
     */
    var oldX, oldY;
    var startX = 50;
    var startY = 50;
    var delta = 2;

    jQuery('*').mousemove(function(e) {

        /**
         * Фиксируем перемещение по горизонтали.
         */
        if (e.clientX > oldX) {
            x = startX + delta;
        } else if (e.clientX === oldX) {
            x = startX;
        } else {
            x = startX - delta;
        }

        oldX = e.clientX;

        if (e.clientY > oldY) {
            y = startY + delta;
        } else if (e.clientY === oldY) {
            y = startY;
        } else {
            y = startY - delta;
        }

        oldY = e.clientY;

        jQuery('body').css('background-position', x + '% ' + y + '%');

    });

    /**
     * Добавление эффектов.
     */
    setTimeout(function () {
        jQuery('#mail-to').fadeIn(5000);
    }, 3000);

    /**
     * Модальное окно.
     */
    jQuery('#mail-to i').click(function (e) {
        jQuery('#modal').modal('show');
    });

    jQuery('#modal').on('shown.bs.modal', function () {
        jQuery('#message-text').trigger('focus');
    })

});
